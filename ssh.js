const os = require('os');
const path = require('path');
const { NodeSSH } = require('node-ssh');
const ssh = new NodeSSH();

// 发布配置
const config = {
  localPath: './build',
  remotePath: '/home/h5/vitae/admin'
};

// 发布文件到服务器
ssh
  .connect({
    host: '139.224.81.47',
    username: 'root',
    privateKey: os.homedir() + '/.ssh/id_rsa'
  })
  .then(function () {
    const failed = [];
    const successful = [];
    ssh
      .putDirectory(config.localPath, config.remotePath, {
        recursive: true,
        concurrency: 10,
        validate: function (itemPath) {
          const baseName = path.basename(itemPath);
          return baseName.slice(0, 1) !== '.' && baseName !== 'node_modules';
        },
        tick: function (localPath, _remotePath, error) {
          if (error) {
            failed.push(localPath);
          } else {
            successful.push(localPath);
          }
        }
      })
      .then(function (status) {
        console.log(status ? '\x1B[32m  代码发布成功\n' : '\x1B[31m  代码发布失败\n');
        if (failed.length) {
          console.log('\x1B[31m× ' + failed.join('\n× '));
        }
        if (successful.length) {
          console.log('\x1B[32m√ ' + successful.join('\n√ '));
        }
        ssh.dispose();
      });
  });
