// 样式
import './assets/css/main.scss';

import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';
import actions from './actions';
import Root from './root';

createRoot(document.getElementById('root')!).render(
  <Provider store={actions}>
    <HashRouter>
      <Root />
    </HashRouter>
  </Provider>
);
