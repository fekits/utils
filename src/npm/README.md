# @fekit/utils

前端常用方法

## 安装

```
yarn add @fekit/utils
```

或

```
npm i @fekit/utils
```

## 引入

```typescript
import { isString, isNumber, isBoolean, isObject, isArray, isNull, isUndefined, isEmpty, isDate, isDateString, isFunction, isElement } from '@fekit/utils';
```

## 使用

```typescript
// 类型判断
import { isString, isNumber, isBoolean, isObject, isArray, isNull, isUndefined, isEmpty, isDate, isDateString, isFunction, isElement } from '@fekit/utils';

// 获取指定对象的类型
getType();

// 判断是否是function
isFunction();

// 判断是否是Object
isObject();

// 判断是否是array
isArray();

// 判断是否是undefined
isUndefined();

// 判断是否是null
isNull();

// 判断是否是字符串
isString();

// 判断是否是布尔类型
isBoolean();

// 判断是否是一个数字
isNumber();

// 判断是否是一个日期
isDate();

// 判断是否是一个日期字符串
isDateString();

// 判断是否是一个DOM节点
isElement();

// 判断是否是一个DOM节点列表
isNodeList();

// 判断是否为空
isEmpty(0); // false
isEmpty(''); // true

// 判断是否为Base64字符串
isBase64();

// 判断是否为Blob对象
isBlob('');

// 判断是不是JSON字符串
isJsonString('{}'); // true

// 判断是不是数组字符串
isArrayString('[1]'); // true
isArrayString('[1}'); // true

// 判断是不是对象字符串
isObjectString('{}'); // true
isObjectString('xxx'); // false

// 日期处理

// 数据对比
```

## 版本

```

```
