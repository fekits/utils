import { isArray, isObject, getType } from './type';

// 对比
export const diff = (lhs: any, rhs: any) => {
  const data: any = [];
  const core = (lhs: any, rhs: any, field: any = '', path = []) => {
    const _path: any = [...path];
    if (field) {
      _path.push(field);
    }
    if (getType(lhs) === getType(rhs)) {
      if (isArray(lhs)) {
        // 判断子元素是否全部是末端
        const isLashArray = [...lhs, ...rhs].every((item: any) => {
          return !isArray(item) && !isObject(item);
        });
        if (isLashArray) {
          if (JSON.stringify(lhs) !== JSON.stringify(rhs)) {
            data.push({ field, lhs, rhs, path });
          }
        } else {
          [...lhs, ...rhs].forEach((_item: any, idx: number) => {
            core(lhs[idx], rhs[idx], `${idx}`, _path);
          });
        }
      } else if (isObject(lhs)) {
        Object.keys({ ...lhs, ...rhs }).forEach((field: any) => {
          core(lhs[field], rhs[field], field, _path);
        });
      } else {
        if (`${lhs}` !== `${rhs}`) {
          data.push({ field, lhs, rhs, path });
        }
      }
    } else {
      if (isArray(lhs) || isObject(lhs) || isObject(rhs) || isObject(rhs)) {
        data.push({ field, lhs, rhs, path });
      } else {
        if (`${lhs}` !== `${rhs}`) {
          data.push({ field, lhs, rhs, path });
        }
      }
    }
    return 1;
  };
  core(lhs, rhs, '');
  return data;
};

// 填充
export const fill = (a: any, b: any): any => {
  if (isArray(a)) {
    const c: any = [];
    const maxLength = Math.max(a.length, b ? b.length : 0);
    for (let i = 0; i < maxLength; i++) {
      c[i] = fill(a[i] !== undefined ? a[i] : isObject(b[0]) ? {} : null, b ? b[i] : isObject(a[0]) ? {} : null);
    }
    return c;
  } else if (isObject(a)) {
    const c: any = {};
    for (const key in a) {
      if (a.hasOwnProperty(key)) {
        c[key] = fill(a[key], b ? b[key] : null);
      }
    }
    for (const key in b) {
      if (b.hasOwnProperty(key) && !c.hasOwnProperty(key)) {
        c[key] = isObject(b[key]) ? fill({}, b[key]) : isArray(b[key]) ? fill([], b[key]) : null;
      }
    }
    return c;
  } else {
    return a !== undefined ? a : null;
  }
};
