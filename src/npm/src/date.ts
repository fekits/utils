import { isDate, isDateString, isNumber, isString } from './type';
import { numfill } from './number';

interface UpdateParam {
  y?: number;
  m?: number;
  d?: number;
}

interface CalendarParam {
  group?: boolean;
  value?: Array<string>;
}

export default class IDate {
  source: any = '';
  attr: any;
  lang: any;
  date: any;
  time: any;
  year: any;
  month: any;
  days: number;
  constructor(lang: any = 'zh') {
    this.lang = lang;
    this.days = 0;
  }
  // 生成日期
  create(obj?: any, opts: any = {}) {
    const zone = opts.zone || '';

    function utc(obj: any) {
      const parts = obj.split(/[-/]/).map(Number);
      const [year, month, day] = parts;
      return new Date(Date.UTC(year, month - 1, day));
    }

    // 如果是10位时间戳
    if (isNumber(obj)) {
      const _len = obj?.toString()?.length;
      if (_len === 10) {
        obj = obj * 1000;
      } else if (_len === 11) {
        obj = obj * 100;
      } else if (_len === 12) {
        obj = obj * 10;
      }
    }
    // 判断是否有入参
    if (obj === undefined) {
      obj = new Date();
    }
    // 判断是否是日期
    if (obj) {
      this.source = obj;
      const date = isDate(obj)
        ? obj
        : isDateString(obj)
        ? isString(obj)
          ? zone === 'utc'
            ? utc(obj)
            : new Date(obj.replace(/-/g, '/'))
          : new Date(obj)
        : new Date();
      this.date = date;
      const cnWeek = ['日', '一', '二', '三', '四', '五', '六'];
      this.time = date.getTime();

      this.attr = {
        time: date.getTime(),
        year: date.getFullYear(),
        YYYY: date.getFullYear(),
        yyyy: date.getFullYear(),
        YY: `${date.getFullYear()}`.slice(-2),
        yy: `${date.getFullYear()}`.slice(-2),
        Y: date.getFullYear(),
        y: date.getFullYear(),
        month: date.getMonth() + 1,
        MM: numfill(date.getMonth() + 1),
        M: date.getMonth() + 1,
        DD: numfill(date.getDate()),
        dd: numfill(date.getDate()),
        D: date.getDate(),
        date: date.getDate(),
        d: date.getDate(),
        H: date.getHours(),
        h: date.getHours() > 12 ? date.getHours() - 12 : date.getHours(),
        HH: numfill(date.getHours()),
        hh: numfill(date.getHours()),
        mm: numfill(date.getMinutes()),
        m: date.getMinutes(),
        ss: numfill(date.getSeconds()),
        s: date.getSeconds(),
        w: date.getDay(),
        W: cnWeek[date.getDay()],
      };
      // 本月天数
      this.days = new Date(this.attr.y, this.attr.M, 0).getDate();
    }
    return this;
  }

  // 日期格式
  format(format = 'YYYY-M-D') {
    if (this.date) {
      return format.replace(/(YYYY|yyyy|YY|yy|MM|M|DD|dd|D|d|HH|hh|H|h|mm|m|ss|s|w)/g, (_: any, key: any) => this.attr[key]);
    } else {
      return this.source;
    }
  }

  // 时间更新
  update(obj: UpdateParam) {
    if (this.date) {
      const { y = 0, m = 0, d = 0, h = 0, n = 0, s = 0 }: any = obj || {};
      if (y) {
        this.date.setFullYear(this.date.getFullYear() + y);
      }
      if (m) {
        this.date.setMonth(this.date.getMonth() + m);
      }
      if (d) {
        this.date.setDate(this.date.getDate() + 1);
      }
      if (h) {
        this.date.setHours(this.date.getHours() + h);
      }
      if (n) {
        this.date.setMinutes(this.date.getMinutes() + n);
      }
      if (s) {
        this.date.setSeconds(this.date.getSeconds() + s);
      }
      this.create(this.date);
    }
    return this;
  }

  // 人性化
  humanized(words: any = {}) {
    if (this.date) {
      const now = new Date();
      const diff = now.getTime() - this.date.getTime();
      const minute = 60 * 1000;
      const hour = 60 * minute;
      const day = 24 * hour;
      const week = 7 * day;
      const month = 30 * day;
      const year = 365 * day;

      const {
        NOW = 'Just now',
        MIN = ' minute ago',
        HRS = ' hour ago',
        DAY = ' day ago',
        WKS = ' week ago',
        MON = ' month ago',
        YRS = ' year ago',
        MINS = ' minutes ago',
        HRSS = ' hours ago',
        DAYS = ' days ago',
        WKSS = ' weeks ago',
        MONS = ' months ago',
        YRSS = ' years ago',
      }: any = words || {};

      if (diff < minute) {
        return NOW;
      } else if (diff < hour) {
        const minutes = Math.floor(diff / minute);
        return `${minutes}${minutes > 1 ? MINS : MIN}`;
      } else if (diff < day) {
        const hours = Math.floor(diff / hour);
        return `${hours}${hours > 1 ? HRSS : HRS}`;
      } else if (diff < week) {
        const days = Math.floor(diff / day);
        return `${days}${days > 1 ? DAYS : DAY}`;
      } else if (diff < month) {
        const weeks = Math.floor(diff / week);
        return `${weeks}${weeks > 1 ? WKSS : WKS}`;
      } else if (diff < year) {
        const months = Math.floor(diff / month);
        return `${months}${months > 1 ? MONS : MON}`;
      } else {
        const years = Math.floor(diff / year);
        return `${years}${years > 1 ? YRSS : YRS}`;
      }
    } else {
      return this.source;
    }
  }

  // 日历
  calendar(param: CalendarParam = {}) {
    if (!this.date) {
      this.create(new Date());
    }
    const { group = false, value = [] }: any = param;
    const values: any = value
      .filter((day: any) => {
        return day && isDateString(day);
      })
      .sort((a: any, b: any) => {
        return idate(a).time - idate(b).time;
      })
      .map((day: any) => idate(day).format('YYYY-MM-DD'));

    // 年
    const y: any = this.date?.getFullYear();
    // 月
    const m: any = this.date?.getMonth() + 1;
    // 本月第一天星期几
    const w = new Date(y, m - 1, 1).getDay();
    // 当前月份一共几天
    const days = new Date(y, m, 0).getDate();

    // 当前
    const curr = new Date();
    const curr_y = curr.getFullYear();
    const curr_m = curr.getMonth() + 1;
    const curr_d = curr.getDate();

    // 上月
    const last = new Date(y, m - 1, 0);
    const last_y = last.getFullYear();
    const last_m = last.getMonth() + 1;
    let prevdays = last.getDate();

    // 下月
    const next = new Date(y, m + 1, 0);
    const next_y = next.getFullYear();
    const next_m = next.getMonth() + 1;
    const _calendar: any = [];

    // 上月的日期
    for (let i = 0; i < w; i++) {
      _calendar.unshift({
        date: `${last_y}-${last_m}-${prevdays}`,
        day: prevdays,
        isLastMonth: true,
      });
      prevdays = prevdays - 1;
    }

    // 本月的日期
    for (let d = 1; d <= days; d++) {
      _calendar.push({
        date: `${y}-${m}-${d}`,
        day: d,
        today: curr_y === y && curr_m === m && curr_d === d,
        isCurrMonth: true,
      });
    }

    // 下月的日期
    for (let i = 1; _calendar.length < 42; i++) {
      const _i = i;
      _calendar.push({
        date: `${next_y}-${next_m}-${_i}`,
        day: _i,
        isNextMonth: true,
      });
    }

    let calendar = _calendar.map((item: any) => {
      const _idx = values.indexOf(idate(item.date).format('YYYY-MM-DD'));
      // 判断是否在选
      if (_idx !== -1) {
        if (item.isCurrMonth) {
          item.selected = true;
        }
        if (_idx === 0) {
          item.sta = true;
        }
        if (_idx === values.length - 1) {
          item.end = true;
        }
      }
      // 判断是否在范围内
      if (values.length > 1) {
        if (item.isCurrMonth && idate(item.date).time >= idate(values[0]).time && idate(item.date).time <= idate(values[values.length - 1]).time) {
          item.range = true;
        }
      }
      return item;
    });
    // 是否按周分组
    if (group) {
      calendar = Array.from({ length: Math.ceil(_calendar.length / 7) }, (_v, index) => _calendar.slice(index * 7, index * 7 + 7));
    }

    return calendar;
  }
}

export const idate = (obj?: any) => new IDate().create(obj);
