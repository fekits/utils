export const numfill = (num: String | Number = '') => {
  return Number(num) < 10 ? `0${num}` : num;
};
