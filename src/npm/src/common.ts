import { isObject, isArray, isFunction, isString } from './type';

// 深拷贝
export const deepcopy = (obj: any) => {
  if (isArray(obj)) {
    const _obj = obj.map((i: any) => {
      return deepcopy(i);
    });
    return _obj;
  } else if (isObject(obj)) {
    const _obj: any = {};
    for (const key in obj) {
      _obj[key] = deepcopy(obj[key]);
    }
    return _obj;
  } else {
    return obj;
  }
};

// 清洗数据
export const cleandata = (obj: any, trim: boolean = true) => {
  const _obj = deepcopy(obj);
  for (const key in _obj) {
    const val = _obj[key];
    if (isArray(val)) {
      _obj[key] = val.filter((item: any) => item !== '' && item !== null && item !== undefined);
      if (!_obj[key].length) {
        delete _obj[key];
      }
    } else if (isObject(val)) {
      _obj[key] = cleandata(val, trim);
    } else {
      if (val === '' || val === null || val === undefined) {
        delete _obj[key];
      } else {
        if (trim && isString(val)) {
          _obj[key] = val.trim();
        }
      }
    }
  }
  return _obj;
};

// 从数据中查找对象
export const findByJson = (enums: any, value: any, key = 'value', child = 'child', chain: any = '', paths: any = ''): any => {
  if (isArray(enums)) {
    // 遍历树结构中的每个节点
    for (const node of enums) {
      const _chain = `${chain ? chain + '/' : ''}${node.label}`;
      const _paths = `${paths ? paths + '/' : ''}${node.value}`;
      // 检查当前节点是否具有所需标签
      if (node[key] === value) {
        node.chain = _chain;
        node.paths = _paths;
        // 如果找到，返回当前节点
        return node;
      }
      // 如果当前节点有子节点，则递归调用该函数
      if (node[child] && node[child]?.length > 0) {
        const result = findByJson(node[child], value, key, child, _chain, _paths);
        if (result) {
          // 如果在子节点中找到，返回结果
          return result;
        }
      }
    }
  }
  // 如果未找到匹配的标签，返回 null
  return null;
};

// 过滤对象中的关键词（无限遍历）
export const filterJson = (json: any, value: any, key: any = ['label'], child: any = 'child') => {
  return deepcopy(json).filter((item: any) => {
    const filteredChild = item[child] && filterJson(item[child], value, key, child);
    const keys: any = isArray(key) ? key : [key];
    const matchesSearch = keys?.some((k: any) => item[k] && item[k].includes(value));
    if (filteredChild && filteredChild.length > 0) {
      item.child = filteredChild;
      return true;
    }
    return matchesSearch;
  });
};

// 数据转枚举结构（无限遍历）
export const dataToEnum = (tree: any, mapping: any) => {
  function core(node: any) {
    const _node: any = {};
    for (const key in mapping) {
      const raw = isFunction(mapping[key]) ? mapping[key](node, tree) : mapping[key];
      const val = node[raw];
      if (isArray(val)) {
        _node[key] = val.map((sub: any) => core(sub));
      } else {
        _node[key] = val;
      }
    }
    return _node;
  }
  return tree.map((item: any) => core(item));
};

// 查找文件类型
export const getFileType = (url: any) => {
  const extension =
    url
      ?.split('.')
      ?.pop()
      ?.toLowerCase() || '';
  if (extension === 'png' || extension === 'jpg' || extension === 'jpeg' || extension === 'gif') {
    return 'image';
  } else if (extension === 'doc' || extension === 'docx') {
    return 'docx';
  } else if (extension === 'xls' || extension === 'xlsx') {
    return 'xlsx';
  } else if (extension === 'ppt' || extension === 'pptx') {
    return 'pptx';
  } else if (extension === 'pdf') {
    return 'pdf';
  } else if (extension === 'html') {
    return 'html';
  } else {
    return 'unknown';
  }
};

// 通过枚举值查找枚举名
export const findEnumName = (enums: any, val: any, join: boolean | string = ' Ι ') => {
  // 转换核心
  const _core = (v: any) => {
    const { label = v }: any = enums.find(({ value = '' }: any) => `${value}` === `${v}`) || {};
    return label;
  };
  // 判断枚举字典是否可用
  if (enums && enums.find) {
    // 兼容多种入参类型
    if (isArray(val)) {
      // 数组
      const arr = val.map((i: any) => _core(i));
      return join ? arr.join(join) : arr;
    } else {
      return _core(val);
    }
  } else {
    return val;
  }
};
