import { isDate, isDateString, isNumber, isString } from './type';
import { idate } from './date';

export const format = {
  // 转换成为大写
  toUpperCase(value: string) {
    if (isString(value)) {
      return value.toUpperCase();
    } else {
      return value;
    }
  },
  // 仅首字母大写
  toFirstUpperCase(value: string) {
    if (isString(value)) {
      return value.charAt(0).toUpperCase() + value.slice(1);
    } else {
      return value;
    }
  },
  // 转换成为小写
  toLowerCase(value: string) {
    if (isString(value)) {
      return value.toLowerCase();
    } else {
      return value;
    }
  },
  // 手机号格式化
  toPhoneNumber(value: string) {
    if (isString(value)) {
      const _value = value
        ?.replace(/\D/g, '')
        ?.substring(0, 11)
        ?.replace(/(\d{0,3})(\d{0,4})(\d{0,4})/, (_, p1, p2, p3) => {
          return [p1, p2, p3].filter(part => part).join('-');
        });
      return _value;
    } else {
      return value;
    }
  },
  // 生成四位一组
  toGroupCase(value: string, len: number) {
    if (isString(value)) {
      let _value = value?.toUpperCase();
      if (_value.length % (len + 1) === 0 && _value.length !== 0 && _value[_value.length - 1] !== '-') {
        _value = _value.slice(0, -1) + '-' + _value.slice(-1);
      }
      _value = _value.replace(/-+/g, '-');
      if (_value[0] === '-') {
        _value = _value.slice(1);
      }
      _value = _value.replace(/[^a-zA-Z0-9-]/g, '');
      if (_value[_value.length - 1] === '-') {
        _value = _value.slice(0, -1);
      }
      return _value;
    }
    return value;
  },
  // 生成日期格式
  toDate(value: string) {
    if (isDate(value) || isDateString(value)) {
      return idate(value).format('YYYY-MM-DD');
    } else {
      return value;
    }
  },
  // 生成时间格式
  toTime(value: string) {
    if (isDate(value) || isDateString(value)) {
      return idate(value).format('HH:mm:ss');
    } else {
      return value;
    }
  },
  // 生成日期时间格式
  toDateTime(value: string) {
    if (isDate(value) || isDateString(value)) {
      return idate(value).format('YYYY-MM-DD HH:mm:ss');
    } else {
      return value;
    }
  },
  // 生成时间戳
  toTimestamp(value: string) {
    if (isDate(value)) {
      return new Date(value).getTime();
    } else {
      return value;
    }
  },
  // 千分位
  toThousands(value: string) {
    const cleaned = value.replace(/\D/g, '');
    const parts = cleaned.match(/^(\d{1,3})((?:\d{3})*)$/);
    if (!parts) {
      return value;
    }
    const firstPart = parts[1];
    const otherParts = parts[2].match(/\d{3}/g);
    let formatted = firstPart;
    if (otherParts) {
      formatted += ',' + otherParts.join(',');
    }
    return formatted;
  },
  // 添加前缀
  toPrefix(value: string, prefix: string) {
    return `${prefix}${value}`;
  },
  // 添加后缀
  toSuffix(value: string, suffix: string) {
    return `${value}${suffix}`;
  },
  // 不够位数补零
  toFillZero(value: string, len: number) {
    if (isString(value) || isNumber(value)) {
      return value.padStart(len, '0');
    } else {
      return value;
    }
  },
  // 截取字符串
  // toSlice(value: string, start: number, end: number) {
  //   return value.slice(start, end);
  // },
  // 添加单位
  // toUnit(value: string, unit: string) {
  //   return `${value}${unit}`;
  // },
  // 转换为货币
  // toCurrency(value: string) {
  //   return parseFloat(value).toFixed(2);
  // },
  // 转换为百分比
  // toPercent(value: string) {
  //   return `${parseFloat(value).toFixed(2)}%`;
  // },
  // 添加小数点
  // toDecimal(value: string, length: number) {
  //   return parseFloat(value).toFixed(length);
  // },
  // 转换为JSON字符串
  // toJSON(value: string) {
  //   return JSON.stringify(value);
  // },
  // 转换为数组
  // toArray(value: string) {
  //   return value.split('');
  // },
  // 转换为对象
  // toObject(value: string) {
  //   return JSON.parse(value);
  // },
  // 转换为布尔值
  // toBoolean(value: string) {
  //   return !!value;
  // },
  // 转换为数字
  // toNumber(value: string) {
  //   return parseFloat(value);
  // },
  // 转换为整数
  // toInteger(value: string) {
  //   return parseInt(value, 10);
  // },
  // 补全网址
  // toURL(value: string) {
  //   return value.startsWith('http') ? value : `https://${value}`;
  // },
};
