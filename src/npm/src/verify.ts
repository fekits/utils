import { isDate, isArray, isFunction, isDateString } from './type';
import { findByJson } from './common';

// 校验
export const verify = (form: any = [], data: any = {}, route: any = {}) => {
  const _err: any = [];
  form?.forEach((item: any = {}) => {
    const { label = '', field = '', rules = [], clear = false } = item || {};
    // 数据查询
    let _obj = data;
    (item.paths?.split('.') || [])?.forEach((i: any) => {
      if (_obj[i]) {
        _obj = _obj[i];
      }
    });
    const value = _obj[field];
    // 表单项ID
    const path = 'form-field-' + field;
    // 表单校验
    if (isFunction(clear) ? !clear(data, form, route) : !clear) {
      (isFunction(rules) ? rules(data, form, route) : rules)?.forEach((rule: any = {}) => {
        const { type = '', msg = '', message = '', required = null, validator = null } = rule;
        if (required) {
          if (isArray(value)) {
            if (!value.length) {
              _err.push({ field, label, type, msg: msg || message, path });
            }
          } else {
            if (!value) {
              _err.push({ field, label, type, msg: msg || message, path });
            } else {
              // 校验值是否在枚举中
              if (item.enums) {
                const _enums = isFunction(item.enums) ? item.enums(data, form, route) : item.enums;
                if (isArray(_enums)) {
                  const _item: any = findByJson(_enums, value);
                  if (_item.value === null || _item.value === undefined) {
                    _err.push({ field, label, type, msg: msg || message, path });
                  }
                }
              }
            }
          }
        }
        if (value && isFunction(validator) && !validator(value)) {
          _err.push({ field, label, type, msg: msg || message, path });
        }
      });
    }
  });
  return _err;
};

// 验证是否为合法时间
export const validateDate = (value: any) => {
  return isDate(value) || isDateString(value);
};
// 判断是否为数字
export const validateNumber = (value: any) => {
  return !isNaN(value);
};
// 验证是否已填写内容
export const validateFilled = (value: any) => {
  if (value === null || value === undefined) {
    return false;
  } else if (Array.isArray(value) && value.length === 0) {
    return false;
  } else if (typeof value === 'string' && value.trim() === '') {
    return false;
  } else {
    return true;
  }
};
// 验证值是否为手机号
export const validatePhoneNumber = (value: string) => {
  return /^1[3456789]\d{9}$/.test(value);
};
// 是否为中国身份证号
export const validateChineseIDCard = (value: string) => {
  return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value);
};
// 验证是否为邮箱地址
export const validateEmail = (value: string) => {
  // return /^[a-zA-Z0-9_-.]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(value);
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value);
};
// 验证是否为中文名字
export const validateChineseName = (value: string) => {
  return /^[\u4e00-\u9fa5]{1,4}$/.test(value);
};
// 验证是否为中文姓名
export const validateZhName = (value: string) => {
  const cnReg = /^(?:[\u4e00-\u9fff]|[\u3400-\u4dbf]|[\uf900-\ufaff]|[\ud840-\ud87f][\udc00-\udfff]){1,4}$/;
  return cnReg.test(value);
};
// 验证是否为英文名字
export const validateEnglishName = (value: string) => {
  return /^[a-zA-Z\s]+$/.test(value);
};
// 验证是否为英文名字
export const validateEnName = (value: string) => {
  return /^[a-zA-Z\s]+$/.test(value);
};
// 验证是否为中国地址
export const validateChineseAddress = (value: any) => {
  return /^[\u4E00-\u9FA5]{2,}(省|市|自治区|特别行政区|地区|盟)([\u4E00-\u9FA5]{2,}(市|区|县)){1,2}([\u4E00-\u9FA5]{2,}(街道|镇|乡)){0,1}([\u4E00-\u9FA5]{2,})?(小区|大厦|村)([\u4E00-\u9FA5]{2,})?$/.test(
    value
  );
};

// 验证是否为年龄
export const validateAge = (value: number) => {
  return !isNaN(value) && value >= 0 && value <= 150;
};

// 验证是否为一个网址
export const validateURL = (url: string) => {
  return /^(https?:\/\/)?([\da-z.-]+)\.([a-z.]{2,6})([/\w .-]*)*\/?$/.test(url);
};

// 校验类型
const types: any = {
  date: validateDate,
  number: validateNumber,
  filled: validateFilled,
  phone: validatePhoneNumber,
  idCard: validateChineseIDCard,
  email: validateEmail,
  chineseName: validateChineseName,
  zhName: validateZhName,
  enName: validateEnglishName,
  chineseAddress: validateChineseAddress,
  age: validateAge,
  url: validateURL,
};

// 通用校验
export const validate = (type: any = '', value: any = null) => {
  if (value) {
    return types[type] ? types[type](value) : false;
  } else {
    return types[type] || (() => false);
  }
};
