/**
 * 对象类型相关判断
 * @module type
 */

/**
 * 获取指定对象的类型
 * @param  {type} obj 	对象
 * @return {string}     对象类型值
 */
export function getType(obj: any): string {
  return Object.prototype.toString.call(obj);
}

/**
 * 判断是否是function
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isFunction(obj: any): boolean {
  return typeof obj === 'function';
}

/**
 * 判断是否是Object
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isObject(obj: any): boolean {
  return '[object Object]' === getType(obj);
}

/**
 * 判断是否是array
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isArray(obj: any): boolean {
  return '[object Array]' === getType(obj);
}

/**
 * 判断是否是undefined
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isUndefined(obj: any): boolean {
  return '[object Undefined]' === getType(obj);
}

/**
 * 判断是否是null
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isNull(obj: any): boolean {
  return '[object Null]' === getType(obj);
}

/**
 * 判断是否是字符串
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isString(obj: any): boolean {
  return typeof obj === 'string';
}

/**
 * 判断是否是布尔类型
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isBoolean(obj: any): boolean {
  return typeof obj === 'boolean';
}

/**
 * 判断是否是一个数字
 * @param  {number}  num 数字
 * @return {Boolean}
 */
export function isNumber(num: any): boolean {
  return '[object Number]' === getType(num) && !isNaN(num);
}

/**
 * 判断是否是一个日期
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isDate(obj: any): boolean {
  return '[object Date]' === getType(obj);
}

/**
 * 判断是否是一个日期字符串
 * @param  {string}  str 字符
 * @return {Boolean}
 */
export function isDateString(str: any): boolean {
  try {
    return new Date(str).toString() !== 'Invalid Date';
  } catch (err) {
    return false;
  }
}

/**
 * 判断是否是一个DOM节点
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isElement(obj: any): boolean {
  return /\[object\sHTML\w*Element\]/.test(getType(obj)) || getType(obj) === '[object HTMLDocument]';
}

/**
 * 判断是否是一个DOM节点列表
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isNodeList(obj: any): boolean {
  return getType(obj) === '[object NodeList]';
}

/**
 * 判断是否为空
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isEmpty(obj: any): boolean {
  if (isArray(obj)) {
    return obj.length === 0;
  } else if (isObject(obj)) {
    return Object.keys(obj).length === 0;
  } else {
    return isUndefined(obj) || isNull(obj) || obj === '';
  }
}

/**
 * 判断是否为Base64字符串
 * @param  {string}  obj 对象
 * @return {Boolean}
 */
export function isBase64(obj: any): boolean {
  return isString(obj) && /data:\w*\/\w*;base64,/.test(obj);
}

/**
 * 判断是否为Blob对象
 * @param  {object}  obj 对象
 * @return {Boolean}
 */
export function isBlob(obj: any): boolean {
  return obj instanceof Blob;
}

/**
 * 判断是不是JSON字符串
 * @param  {object}  obj 任意类型
 * @return {Boolean}
 */
export function isJsonString(obj: any): boolean {
  try {
    JSON.parse(obj);
    return true;
  } catch (e) {
    return false;
  }
}

/**
 * 判断是不是数组字符串
 * @param  {object}  obj 任意类型
 * @return {Boolean}
 */
export function isArrayString(obj: any): boolean {
  try {
    const a = JSON.parse(obj);
    return isArray(a);
  } catch (e) {
    return false;
  }
}

/**
 * 判断是不是对象字符串
 * @param  {object}  obj 任意类型
 * @return {Boolean}
 */
export function isObjectString(obj: any): boolean {
  try {
    const o = JSON.parse(obj);
    return isObject(o);
  } catch (e) {
    return false;
  }
}

/**
 * 判断是不是经过编码的字符串
 * @param  {object}  str 字符串
 * @return {Boolean}
 */
export const isEncoded = (str: any) => {
  try {
    return str !== decodeURIComponent(str);
  } catch (_e) {
    // 如果解码失败，说明字符串未被正确编码
    return false;
  }
};
