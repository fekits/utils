import React from 'react';
import { connect } from 'react-redux';
import { isString, isNumber, isBoolean, isObject, isArray, isNull, isUndefined, isEmpty, isDate, isFunction, isElement, idate } from './npm/src';

const store = ({ _sys = {} }: any) => ({ _sys });
const event = (dispatch: any) => {
  return {};
};
function Root(props: any) {
  console.log(10, idate('2021-2-2').format('我是一个日期，{yyyy}年或者{yy}年{M}月的{D}日或2位数的{DD}日'));
  console.log(11, '2023-5-2', idate('2023-5-2').humanized());
  console.log(11, '2023-5-26', idate('2023-5-26').humanized());
  console.log(11, '2021-5-30', idate('2023-5-30 15:33:00').humanized());
  console.log(11, '2021-5-30', idate('2023-5-30 01:00:00').humanized());
  console.log(11, '2023-4-10', idate('2023-4-10').humanized());
  console.log(11, '2023-4-30', idate('2023-4-30').humanized());
  console.log(11, '2021-2-2', idate('2021-2-2').humanized());
  console.log(11, '2021-2-2', idate('2021-2-2').humanized());
  console.log(11, '2021-2-2', idate('2021-2-2').humanized());

  console.log(12, idate('').calendar());

  return (
    <div className="ny-sm-sub">
      <div>
        <h5>isString</h5>
        <pre>isString('') =&gt; {isString('') ? 'true' : 'false'}</pre>
        <pre>isString(0) =&gt; {isString(0) ? 'true' : 'false'}</pre>
        <pre>isString(true) =&gt; {isString(true) ? 'true' : 'false'}</pre>
        <pre>isString(null) =&gt; {isString(null) ? 'true' : 'false'}</pre>
        <pre>isString(undefined) =&gt; {isString(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isString({'{a:1}'}) =&gt; {isString({}) ? 'true' : 'false'}
        </pre>
        <pre>isString([1,2,3]) =&gt; {isString([]) ? 'true' : 'false'}</pre>
        <pre>isString(new Date()) =&gt; {isString(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isString({'()=>{}'}) =&gt; {isString(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isString({'document'}) =&gt; {isString(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isNumber</h5>
        <pre>isNumber('') =&gt; {isNumber('') ? 'true' : 'false'}</pre>
        <pre>isNumber(0) =&gt; {isNumber(0) ? 'true' : 'false'}</pre>
        <pre>isNumber(true) =&gt; {isNumber(true) ? 'true' : 'false'}</pre>
        <pre>isNumber(null) =&gt; {isNumber(null) ? 'true' : 'false'}</pre>
        <pre>isNumber(undefined) =&gt; {isNumber(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isNumber({'{a:1}'}) =&gt; {isNumber({}) ? 'true' : 'false'}
        </pre>
        <pre>isNumber([1,2,3]) =&gt; {isNumber([]) ? 'true' : 'false'}</pre>
        <pre>isNumber(new Date()) =&gt; {isNumber(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isNumber({'()=>{}'}) =&gt; {isNumber(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isNumber({'document'}) =&gt; {isNumber(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isBoolean</h5>
        <pre>isBoolean('') =&gt; {isBoolean('') ? 'true' : 'false'}</pre>
        <pre>isBoolean(0) =&gt; {isBoolean(0) ? 'true' : 'false'}</pre>
        <pre>isBoolean(true) =&gt; {isBoolean(true) ? 'true' : 'false'}</pre>
        <pre>isBoolean(null) =&gt; {isBoolean(null) ? 'true' : 'false'}</pre>
        <pre>isBoolean(undefined) =&gt; {isBoolean(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isBoolean({'{a:1}'}) =&gt; {isBoolean({}) ? 'true' : 'false'}
        </pre>
        <pre>isBoolean([1,2,3]) =&gt; {isBoolean([]) ? 'true' : 'false'}</pre>
        <pre>isBoolean(new Date()) =&gt; {isBoolean(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isBoolean({'()=>{}'}) =&gt; {isBoolean(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isBoolean({'document'}) =&gt; {isBoolean(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isObject</h5>
        <pre>isObject('') =&gt; {isObject('') ? 'true' : 'false'}</pre>
        <pre>isObject(0) =&gt; {isObject(0) ? 'true' : 'false'}</pre>
        <pre>isObject(true) =&gt; {isObject(true) ? 'true' : 'false'}</pre>
        <pre>isObject(null) =&gt; {isObject(null) ? 'true' : 'false'}</pre>
        <pre>isObject(undefined) =&gt; {isObject(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isObject({'{a:1}'}) =&gt; {isObject({}) ? 'true' : 'false'}
        </pre>
        <pre>isObject([1,2,3]) =&gt; {isObject([]) ? 'true' : 'false'}</pre>
        <pre>isObject(new Date()) =&gt; {isObject(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isObject({'()=>{}'}) =&gt; {isObject(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isObject({'document'}) =&gt; {isObject(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isArray</h5>
        <pre>isArray('') =&gt; {isArray('') ? 'true' : 'false'}</pre>
        <pre>isArray(0) =&gt; {isArray(0) ? 'true' : 'false'}</pre>
        <pre>isArray(true) =&gt; {isArray(true) ? 'true' : 'false'}</pre>
        <pre>isArray(null) =&gt; {isArray(null) ? 'true' : 'false'}</pre>
        <pre>isArray(undefined) =&gt; {isArray(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isArray({'{a:1}'}) =&gt; {isArray({}) ? 'true' : 'false'}
        </pre>
        <pre>isArray([1,2,3]) =&gt; {isArray([]) ? 'true' : 'false'}</pre>
        <pre>isArray(new Date()) =&gt; {isArray(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isArray({'()=>{}'}) =&gt; {isArray(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isArray({'document'}) =&gt; {isArray(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isNull</h5>
        <pre>isNull('') =&gt; {isNull('') ? 'true' : 'false'}</pre>
        <pre>isNull(0) =&gt; {isNull(0) ? 'true' : 'false'}</pre>
        <pre>isNull(true) =&gt; {isNull(true) ? 'true' : 'false'}</pre>
        <pre>isNull(null) =&gt; {isNull(null) ? 'true' : 'false'}</pre>
        <pre>isNull(undefined) =&gt; {isNull(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isNull({'{a:1}'}) =&gt; {isNull({}) ? 'true' : 'false'}
        </pre>
        <pre>isNull([1,2,3]) =&gt; {isNull([]) ? 'true' : 'false'}</pre>
        <pre>isNull(new Date()) =&gt; {isNull(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isNull({'()=>{}'}) =&gt; {isNull(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isNull({'document'}) =&gt; {isNull(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isUndefined</h5>
        <pre>isUndefined('') =&gt; {isUndefined('') ? 'true' : 'false'}</pre>
        <pre>isUndefined(0) =&gt; {isUndefined(0) ? 'true' : 'false'}</pre>
        <pre>isUndefined(true) =&gt; {isUndefined(true) ? 'true' : 'false'}</pre>
        <pre>isUndefined(null) =&gt; {isUndefined(null) ? 'true' : 'false'}</pre>
        <pre>isUndefined(undefined) =&gt; {isUndefined(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isUndefined({'{a:1}'}) =&gt; {isUndefined({}) ? 'true' : 'false'}
        </pre>
        <pre>isUndefined([1,2,3]) =&gt; {isUndefined([]) ? 'true' : 'false'}</pre>
        <pre>isUndefined(new Date()) =&gt; {isUndefined(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isUndefined({'()=>{}'}) =&gt; {isUndefined(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isUndefined({'document'}) =&gt; {isUndefined(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isEmpty</h5>
        <pre>isEmpty('') =&gt; {isEmpty('') ? 'true' : 'false'}</pre>
        <pre>isEmpty(0) =&gt; {isEmpty(0) ? 'true' : 'false'}</pre>
        <pre>isEmpty(true) =&gt; {isEmpty(true) ? 'true' : 'false'}</pre>
        <pre>isEmpty(null) =&gt; {isEmpty(null) ? 'true' : 'false'}</pre>
        <pre>isEmpty(undefined) =&gt; {isEmpty(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isEmpty({'{a:1}'}) =&gt; {isEmpty({}) ? 'true' : 'false'}
        </pre>
        <pre>isEmpty([1,2,3]) =&gt; {isEmpty([]) ? 'true' : 'false'}</pre>
        <pre>isEmpty(new Date()) =&gt; {isEmpty(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isEmpty({'()=>{}'}) =&gt; {isEmpty(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isEmpty({'document'}) =&gt; {isEmpty(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isDate</h5>
        <pre>isDate('') =&gt; {isDate('') ? 'true' : 'false'}</pre>
        <pre>isDate(0) =&gt; {isDate(0) ? 'true' : 'false'}</pre>
        <pre>isDate(true) =&gt; {isDate(true) ? 'true' : 'false'}</pre>
        <pre>isDate(null) =&gt; {isDate(null) ? 'true' : 'false'}</pre>
        <pre>isDate(undefined) =&gt; {isDate(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isDate({'{a:1}'}) =&gt; {isDate({}) ? 'true' : 'false'}
        </pre>
        <pre>isDate([1,2,3]) =&gt; {isDate([]) ? 'true' : 'false'}</pre>
        <pre>isDate(new Date()) =&gt; {isDate(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isDate({'()=>{}'}) =&gt; {isDate(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isDate({'document'}) =&gt; {isDate(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isFunction</h5>
        <pre>isFunction('') =&gt; {isFunction('') ? 'true' : 'false'}</pre>
        <pre>isFunction(0) =&gt; {isFunction(0) ? 'true' : 'false'}</pre>
        <pre>isFunction(true) =&gt; {isFunction(true) ? 'true' : 'false'}</pre>
        <pre>isFunction(null) =&gt; {isFunction(null) ? 'true' : 'false'}</pre>
        <pre>isFunction(undefined) =&gt; {isFunction(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isFunction({'{a:1}'}) =&gt; {isFunction({}) ? 'true' : 'false'}
        </pre>
        <pre>isFunction([1,2,3]) =&gt; {isFunction([]) ? 'true' : 'false'}</pre>
        <pre>isFunction(new Date()) =&gt; {isFunction(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isFunction({'()=>{}'}) =&gt; {isFunction(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isFunction({'document'}) =&gt; {isFunction(document) ? 'true' : 'false'}
        </pre>
      </div>
      <div>
        <h5>isElement</h5>
        <pre>isElement('') =&gt; {isElement('') ? 'true' : 'false'}</pre>
        <pre>isElement(0) =&gt; {isElement(0) ? 'true' : 'false'}</pre>
        <pre>isElement(true) =&gt; {isElement(true) ? 'true' : 'false'}</pre>
        <pre>isElement(null) =&gt; {isElement(null) ? 'true' : 'false'}</pre>
        <pre>isElement(undefined) =&gt; {isElement(undefined) ? 'true' : 'false'}</pre>
        <pre>
          isElement({'{a:1}'}) =&gt; {isElement({}) ? 'true' : 'false'}
        </pre>
        <pre>isElement([1,2,3]) =&gt; {isElement([]) ? 'true' : 'false'}</pre>
        <pre>isElement(new Date()) =&gt; {isElement(new Date()) ? 'true' : 'false'}</pre>
        <pre>
          isElement({'()=>{}'}) =&gt; {isElement(() => {}) ? 'true' : 'false'}
        </pre>
        <pre>
          isElement({'document.querySelector("div")'}) =&gt; {isElement(document.querySelector('div')) ? 'true' : 'false'}
        </pre>
      </div>
    </div>
  );
}

export default connect(store, event)(React.memo(Root));
