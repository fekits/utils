import store from '@fekit/react-store';
// 模块
import * as _sys from './_sys';
import * as base from './base';

const actions: any = {
  _sys,
  base
};

export default store(actions);
