// 默认数据
export default {
  // 系统致命错误
  fail: 0,
  // 侧栏菜单开关
  side: window.innerWidth < 800 ? 'hide' : 'open',
  // 当前系统语言
  lang: window.localStorage.getItem('YDK_ADMIN_LANG') || 'zh-CN',
  // 当前系统字号
  size: window.localStorage.getItem('YDK_ADMIN_SIZE') || 'm',
  // 当前系统主题
  view: window.localStorage.getItem('YDK_ADMIN_VIEW') || 'auto'
};

// 设置系统语言
export function _SYS_SET_LANG(state: any, lang: any) {
  if (lang) {
    state.lang = lang;
    window.localStorage.setItem('YDK_ADMIN_LANG', lang);
  }
  return { ...state };
}

// 设置系统字号
export function _SYS_SET_SIZE(state: any, size: any) {
  if (size) {
    state.size = size;
    window.localStorage.setItem('YDK_ADMIN_SIZE', size);
  }
  return { ...state };
}

// 设置系统主题
export function _SYS_SET_VIEW(state: any, { view = '' }) {
  console.log(view);
  if (view) {
    state.view = view;
    window.localStorage.setItem('YDK_ADMIN_VIEW', view);
  }
  return { ...state };
}
